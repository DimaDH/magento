<?php


$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('banners/bannerspage'))
    ->addColumn('banners_id' , Varien_Db_Ddl_Table::TYPE_INTEGER, null ,array(
        'identify' => true,
        'auto_increment' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true
    ), 'Banners ID')
    ->addColumn('title', Varien_Db_Ddl_Table::TYPE_TEXT, array(
        'nullable' => false,
    ), 'Title')
    ->addColumn('url', Varien_Db_Ddl_Table::TYPE_TEXT, array(
        'nullable' => false,
    ), 'Url')
    ->addColumn('image', Varien_Db_Ddl_Table::TYPE_TEXT, array(
        'nullable' => false,
    ), 'Image')
    ->addColumn('position', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
        'nullable' => true,
    ), 'Position')
    ->addColumn('banners_status', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
        'nullable' => false,
    ), 'Status');

$installer->getConnection()->createTable($table);

$installer->endSetup();