<?php


class RonisBT_Banners_Block_Adminhtml_Banners_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'banners_id';
        $this->_controller = 'adminhtml_banners';
        $this->_blockGroup = 'banners';

        parent::__construct();
        $this->_updateButton('save', 'label', 'Save Image');
        $this->_updateButton('delete', 'label', 'Delete Image');

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "


            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }


    public function getHeaderText()
    {
        if (Mage::registry('banners_block')->getId()) {
            return Mage::helper('contacts')->__(" Banners Form '%s'", $this->escapeHtml(Mage::registry('banners_block')->getTitle()));
        }
        else {
            return 'New Block';
        }
    }
}
