<?php


class RonisBT_Banners_Block_Adminhtml_Banners_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('cmsBlockGrid');
        $this->setDefaultSort('block_identifier');
        $this->setDefaultDir('DESC');
    }
    

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('banners/bannerspage')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('Number', array(
            'header' => 'Number',
            'align' => 'left',
            'index' => 'banners_id',
        ));

        $this->addColumn('title', array(
            'header' => 'Title',
            'align' => 'left',
            'index' => 'title',
        ));

        $this->addColumn('url', array(
            'header' => 'Url',
            'align' => 'left',
            'index' => 'url',
        ));


        $this->addColumn('position', array(
            'header' => 'Position',
            'index' => 'position',

        ));

        $this->addColumn('banners_status' , array(
            'header' => 'Status',
            'index' => 'banners_status'
        ));


        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('banners_id' => $row->getId()));
    }
}
