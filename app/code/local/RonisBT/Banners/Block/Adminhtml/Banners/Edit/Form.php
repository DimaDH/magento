<?php


class RonisBT_Banners_Block_Adminhtml_Banners_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('block_form');
        $this->setTitle('Create Banners');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('banners/bannerspage')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }


    protected function _prepareForm()
    {
        $model = Mage::registry('banners_block');
        $form = new Varien_Data_Form(
            array(
                'id' => 'edit_form',
                'action' => $this->getUrl('*/*/save',array('banners_id'=>$this->getRequest()->getParam('banners_id'))),
                'method' => 'post',
                'enctype' => 'multipart/form-data'
            )
        );

        $form->setHtmlIdPrefix('banners_');


        $fieldset = $form->addFieldset('base_fieldset', array('legend'=>'Add image to banners', 'class' => 'fieldset-wide'));

        if ($model->getBlockId()) {
            $fieldset->addField('banners_id', 'hidden', array(
                'name' => 'banners_id',
            ));
        }

        $fieldset->addField('title', 'text', array(
            'name'      => 'title',
            'label'     => 'Title',
            'title'     => 'title',
            'required'  => true,
        ));

        $fieldset->addField('url', 'text', array(
            'name'      => 'url',
            'label'     => 'Url',
            'title'     => 'url',
            'required'  => true,
        ));
        $fieldset->addType('bannerimage', 'RonisBT_Banners_Block_Adminhtml_Banners_Edit_Renderer_Bannerimage');
        $fieldset->addField('image', 'bannerimage', array(
            'name'      => 'image',
            'label'     => 'Image',
            'title'     => 'image',
            'required'  => true,
        ));

        $fieldset->addField('position', 'select', array(
            'name'      => 'position',
            'label'     => 'position',
            'title'     => 'position',
            'values' => array(
                '1' => '1',
                '2' => '2',
                '3' => '3',
            ),
            'required'  => true,
        ));

        $fieldset->addField('banners_status', 'select', array(
            'label'     => 'title',
            'title'     => 'title',
            'name'      => 'banners_status',
            'values' => array(
                'enable' => 'enable',
                'disable' => 'disable',
            ),
            'required'  => true,
        ));

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}