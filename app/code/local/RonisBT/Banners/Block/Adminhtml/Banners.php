<?php


class RonisBT_Banners_Block_Adminhtml_Banners extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_controller = 'adminhtml_banners';
        $this->_blockGroup = 'banners';
        $this->_headerText = 'Home Page Banners';
        $this->_addButtonLabel = 'Add New Image';
        parent::__construct();
    }

}