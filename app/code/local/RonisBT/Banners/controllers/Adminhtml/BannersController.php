<?php


class RonisBT_Banners_Adminhtml_BannersController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('banners/adminhtml_banners'));
        $this->renderLayout();
    }
    
    public function newAction()
    {
        $this->_forward('edit');
    }

    public function deleteAction()
    {
        if( $this->getRequest()->getParam('banners_id') > 0 ) {
            try {

                $block = Mage::getModel('banners/bannerspage')->load($this->getRequest()->getParam('banners_id'));

                $path = Mage::getBaseDir('media') . DS . 'banners' . DS;

                unlink($path.$block['image']);

                $block->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Image was deleted successfully!'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }
    
    protected function _uploadFile($fieldName,$model)
    {

        if( ! isset($_FILES[$fieldName])) {
            return false;
        }
        $file = $_FILES[$fieldName];

        if(isset($file['name']) && (file_exists($file['tmp_name']))){
            if($model->getId()){
                unlink(Mage::getBaseDir('media').DS.$model->getData($fieldName));
            }
            try
            {
                $path = Mage::getBaseDir('media') . DS . 'banners' . DS;
                $uploader = new Varien_File_Uploader($file);

                $uploader->setAllowedExtensions(array('jpg','png','gif','jpeg'));
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(false);
                $uploader->save($path, $file['name']);
                $model->setData($fieldName,$uploader->getUploadedFileName());
                return true;
            }
            catch(Exception $e)
            {
                return false;
            }
        }
    }


    public function editAction()
    {
        $id = $this->getRequest()->getParam('banners_id');
        Mage::register('banners_block', Mage::getModel('banners/bannerspage')->load($id));
        $blockObject = (array)Mage::getSingleton('adminhtml/session')->getBlockObject(true);
        if (count($blockObject)) {
            Mage::registry('banners_block')->setData($blockObject);
        }
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('banners/adminhtml_banners_edit'));
        $this->renderLayout();
    }

    public function saveAction()
    {
        try {
            $id = $this->getRequest()->getParam('banners_id');
            $block = Mage::getModel('banners/bannerspage')->load($id);
            $block
                ->setData($this->getRequest()->getParams());
                $this->_uploadFile('image', $block);
            $block
                ->save();

            if(!$block->getId()) {
                Mage::getSingleton('adminhtml/session')->addError('Cannot save the image');
            }
        } catch(Exception $e) {
            Mage::logException($e);
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::getSingleton('adminhtml/session')->setBlockObject($block->getData());
            return  $this->_redirect('*/*/edit',array('banners_id'=>$this->getRequest()->getParam('banners_id')));
        }

        Mage::getSingleton('adminhtml/session')->addSuccess('Image was saved successfully!');

        $this->_redirect('*/*/'.$this->getRequest()->getParam('back','index'),array('banners_id'=>$block->getId()));
    }
}

