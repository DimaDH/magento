<?php

class RonisBT_Banners_IndexController extends Mage_Core_Controller_Front_Action
{

    public function IndexAction() {

    $this->loadLayout();
    $this->getLayout()->getBlock("head")->setTitle($this->__("Banners"));
    $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
    $breadcrumbs->addCrumb("home", array(
        "label" => $this->__("Home Page"),
        "title" => $this->__("Home Page"),
        "link"  => Mage::getBaseUrl()
    ));
    $breadcrumbs->addCrumb("Banners", array(
        "label" => $this->__("Banners"),
        "title" => $this->__("Banners")
    ));
    $this->renderLayout();

    }

    public function ClickAction()
    {
    $id = $this->getRequest()->getParam("banners_id");

    $model = Mage::getModel("banners/bannerspage")->load($id);
    if ($model->getId()) {
        $click = (int)$model->getClicks();
        $click += 1;
        $model->setClicks($click);
        $model->save();
        $this->getResponse()->setRedirect($model->getUrl());
        return;
        }
    }
}