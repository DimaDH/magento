<?php

class Ronis_Feedback_Model_Resource_Contactus extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('feedback/contactus', 'contact_id');
    }
}