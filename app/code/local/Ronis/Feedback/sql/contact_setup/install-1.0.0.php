<?php


$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('feedback/contactus'))
    ->addColumn('contact_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identify' => true,
        'auto_increment' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true
    ), 'Contact ID')
    ->addColumn('customer', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
        'nullable' => true,
    ), 'Customer')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
        'nullable' => false,
    ), 'Name')
    ->addColumn('email', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
        'nullable' => false,
    ), 'Email')
    ->addColumn('phone', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
        'nullable' => true,
    ), 'Telephone')
    ->addColumn('subject', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
        'nullable' => false,
    ), 'Subject')
    ->addColumn('other_subject', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
        'nullable' => true,
    ), 'Other Subject')
    ->addColumn('message', Varien_Db_Ddl_Table::TYPE_TEXT, array(
        'nullable' => true,
    ), 'Message')
    ->addColumn('message_status', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
        'nullable' => true,
    ), 'Message Status')
    ->addColumn('user_agent', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable' => true,
    ), 'User_Agent')
    ->addColumn('remote_ip', Varien_Db_Ddl_Table::TYPE_TEXT, 32, array(
        'nullable' => true,
    ), 'Remote IP')
    ->addColumn('created_message', Varien_Db_Ddl_Table::TYPE_DATETIME, array(
        'nullable' => true,
    ), 'Message Created Time')
    ->addColumn('update_created_message', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, array(
        'nullable' => true,
    ), 'Message Created Time');


$installer->getConnection()->createTable($table);

$installer->endSetup();



