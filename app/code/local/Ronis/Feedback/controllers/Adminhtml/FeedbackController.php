<?php


class Ronis_Feedback_Adminhtml_FeedbackController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('feedback/adminhtml_contactForm'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        $this->_redirect('*/*/');

    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('contact_id');
        Mage::register('feedback_block', Mage::getModel('feedback/contactus')->load($id));
        $blockObject = (array)Mage::getSingleton('adminhtml/session')->getBlockObject(true);
        if (count($blockObject)) {
            Mage::registry('feedback_block')->setData($blockObject);
        }
        $blocks = Mage::getModel('feedback/contactus')
            ->getCollection()
            ->addFieldToFilter('contact_id', array('in' => $id));
        foreach ($blocks as $block) {
            $block->setMessageStatus('read')->save();
        }
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('feedback/adminhtml_feedback_edit'));
        $this->renderLayout();
    }

    public function saveAction()
    {

        try {
            $id = $this->getRequest()->getParam('contact_id');
            $block = Mage::getModel('feedback/contactus')->load($id);
            $block
                ->setData($this->getRequest()->getParams())
                ->addData(array('update_created_message' => date('Y-m-d H:i:s', time())))
                ->save();

            if (!$block->getId()) {
                Mage::getSingleton('adminhtml/session')->addError('Cannot save the block');
            }
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::getSingleton('adminhtml/session')->setBlockObject($block->getData());
            return $this->_redirect('*/*/edit', array('contact_id' => $this->getRequest()->getParam('contact_id')));
        }

        Mage::getSingleton('adminhtml/session')->addSuccess('Block was saved successfully!');

        $this->_redirect('*/*/' . $this->getRequest()->getParam('back', 'index'), array('contact_id' => $block->getId()));
    }

    public function massStatusAction()
    {
        $statuses = $this->getRequest()->getParams();
        try {
            $blocks = Mage::getModel('feedback/contactus')
                ->getCollection()
                ->addFieldToFilter('contact_id', array('in' => $statuses['massaction']));
            foreach ($blocks as $block) {
                $block->setMessageStatus($statuses['message_status']);
            }
            $blocks->save();
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            return $this->_redirect('*/*/');
        }
        Mage::getSingleton('adminhtml/session')->addSuccess('Status were updated!');

        return $this->_redirect('*/*/');

    }

    public function massDeleteAction()
    {
        $blocks = $this->getRequest()->getParams();
        try {
            $blocks = Mage::getModel('feedback/contactus')
                ->getCollection()
                ->addFieldToFilter('contact_id', array('in' => $blocks['massaction']));
            $blocks->walk('delete');
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            return $this->_redirect('*/*/');
        }
        Mage::getSingleton('adminhtml/session')->addSuccess('Message were deleted!');

        return $this->_redirect('*/*/');

    }
}