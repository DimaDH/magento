<?php


class Ronis_Feedback_IndexController extends Mage_Core_Controller_Front_Action
{
    const XML_PATH_EMAIL_RECIPIENT = 'feedback/email/recipient_email';
    const XML_PATH_EMAIL_SENDER = 'feedback/email/sender_email_identity';
    const XML_PATH_EMAIL_TEMPLATE = 'feedback/email/email_template';
    const XML_PATH_ENABLED = 'feedback/feedback/enabled';

    public function preDispatch()
    {
        parent::preDispatch();

        if (!Mage::getStoreConfigFlag(self::XML_PATH_ENABLED)) {
            $this->norouteAction();
        }
    }

    public function indexAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('contactForm')
            ->setFormAction(Mage::getUrl('*/*/save', array('_secure' => $this->getRequest()->isSecure())));
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
    }

    public function saveAction()
    {
        $formId = 'contact_page_captcha';
        $captchaModel = Mage::helper('captcha')->getCaptcha($formId);
        $post = $this->getRequest()->getPost();
        if ($captchaModel->isRequired() || Mage::getSingleton('customer/session')->isLoggedIn()) {
            if ($post) {
                $translate = Mage::getSingleton('core/translate');
                $translate->setTranslateInline(false);
                try {

                    $postObject = new Varien_Object();
                    $postObject->setData($post);

                    $error = false;

                    if (!Zend_Validate::is(trim($post['name']), 'NotEmpty')) {
                        $error = true;
                    }

                    if (!Zend_Validate::is(trim($post['subject']), 'NotEmpty')) {
                        $error = true;
                    }

                    if (!Zend_Validate::is(trim($post['message']), 'NotEmpty')) {
                        $error = true;
                    }

                    if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                        $error = true;
                    }
                    if ($captchaModel->isRequired()) {
                        if (!$captchaModel->isCorrect($this->_getCaptchaString($this->getRequest(), $formId))) {
                            $this->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
                            $error = true;
                        }
                    }

                    if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
                        $error = true;
                    }

                    if ($error) {
                        throw new Exception();
                    }

                    $message = Mage::getModel('feedback/contactus')->load();
                    $message
                        ->addData($post)
                        ->addData(array('user_agent' => Mage::helper('core/http')->getHttpUserAgent($clean = true)))
                        ->addData(array('message_status' => 'unread'))
                        ->addData(array('remote_ip' => Mage::helper('core/http')->getRemoteAddr($ipToLong = false)))
                        ->addData(array('customer' => Mage::getSingleton('customer/session')->getId()))
                        ->addData(array('created_message' => date('Y-m-d H:i:s', time())))
                        ->save();

                    $mailTemplate = Mage::getModel('core/email_template');
                    $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                        ->setReplyTo($post['email'])
                        ->sendTransactional(
                            Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE),
                            Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
                            Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT),
                            null,
                            array('data' => $postObject)
                        );

                    if (!$mailTemplate->getSentSuccess()) {
                        throw new Exception();
                    }


                    $translate->setTranslateInline(true);
                    Mage::getSingleton('customer/session')->addSuccess('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.');
                    Mage::getSingleton('customer/session')->setFeedbackData('');
                    $this->_redirect('*/*/');
                    return;

                } catch
                (Exception $e) {
                    $translate->setTranslateInline(true);
                    Mage::logException($e);
                    Mage::getSingleton('customer/session')->addError('Unable to submit your request. Please, try again later');
                    Mage::getSingleton('customer/session')->setFeedbackData($post);
                    return $this->_redirect('*/*/', array('contact_id' => $this->getRequest()->getParam('contact_id')));
                }
            } else {
                $this->_redirect('*/*/');
            }
        }
    }

    protected function _getCaptchaString($request, $formId)
    {
        $captchaParams = $request->getPost(Mage_Captcha_Helper_Data::INPUT_NAME_FIELD_VALUE);
        return $captchaParams[$formId];
    }
}