<?php


class Ronis_Feedback_Block_Adminhtml_Feedback_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('cmsBlockGrid');
        $this->setDefaultSort('block_identifier');
        $this->setDefaultDir('DESC');
    }



    protected function _prepareCollection()
    {
        $collection = Mage::getModel('feedback/contactus')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('contact_id', array(
            'header' => 'Number of Message',
            'align' => 'left',
            'index' => 'contact_id',
        ));

        $this->addColumn('name', array(
            'header' => 'Name',
            'align' => 'left',
            'index' => 'name',
        ));

        $this->addColumn('message_status', array(
            'header' => 'Message Status',
            'align' => 'left',
            'index' => 'message_status',
        ));


        $this->addColumn('subject', array(
            'header' => 'Subject',
            'index' => 'subject',

        ));


        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('contact_id');
        $this->getMassactionBlock()->setIdFieldName('contact_id');

        $this->getMassactionBlock()
            ->addItem('delete',
                array(
                    'label' => 'Delete',
                    'url' => $this->getUrl('*/*/massDelete'),
                    'confirm' => 'Are you sure?'
                )
            )
            ->addItem('status',
                array(
                    'label' => 'Update status',
                    'url' => $this->getUrl('*/*/massStatus'),
                    'additional' =>
                        array('message_status' =>
                            array(
                                'name' => 'message_status',
                                'type' => 'select',
                                'class' => 'required-entry',
                                'label' => 'Status',
                                'values' => array(
                                    'unread' => 'unread',
                                    'read' => 'read',
                                    'spam' => 'spam',
                                )
                            )
                        )
                )
            );

        return $this;
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('contact_id' => $row->getId()));
    }
}
