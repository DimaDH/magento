<?php


class Ronis_Feedback_Block_Adminhtml_Feedback_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'contact_id';
        $this->_controller = 'adminhtml_feedback';
        $this->_blockGroup = 'feedback';

        parent::__construct();
    }
    

    public function getHeaderText()
    {
        if (Mage::registry('feedback_block')->getId()) {
            return Mage::helper('contacts')->__("Message Form", $this->escapeHtml(Mage::registry('feedback_block')->getTitle()));

        } else {
            return 'New Block';
        }
    }
}
