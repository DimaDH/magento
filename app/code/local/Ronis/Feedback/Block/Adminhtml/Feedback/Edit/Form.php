<?php


class Ronis_Feedback_Block_Adminhtml_Feedback_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('block_form');
        $this->setTitle('Message Information');
    }
    

    protected function _prepareForm()
    {
        $model = Mage::registry('feedback_block');
        $form = new Varien_Data_Form(
            array(
                'id' => 'edit_form',
                'action' => $this->getUrl('*/*/save', array('contact_id' => $this->getRequest()->getParam('contact_id'))),
                'method' => 'post'
            )
        );

        $form->setHtmlIdPrefix('feedback_');

        $fieldset = $form->addFieldset('base_fieldset', array('legend' => Mage::helper('contacts')->__('General Information'), 'class' => 'fieldset-wide'));


        $fieldset->addField('customer', 'text', array(
            'name' => 'customer',
            'label' => 'Customer Id',
            'title' => 'Customer Id',
            'disabled' => true,
        ));

        $fieldset->addField('name', 'text', array(
            'label' => 'Name',
            'title' => 'Name',
            'name' => 'name',
            'disabled' => true,
        ));

        $fieldset->addField('email', 'text', array(
            'label' => 'Email',
            'title' => 'Email',
            'name' => 'email',
            'disabled' => true,
        ));


        $fieldset->addField('phone', 'text', array(
            'label' => 'phone',
            'title' => 'phone',
            'name' => 'phone',
            'disabled' => true,
        ));

        $fieldset->addField('user_agent', 'text', array(
            'name' => 'user_agent',
            'label' => 'User Agent',
            'title' => 'User Agent',
            'disabled' => true,
        ));


        $fieldset->addField('remote_ip', 'text', array(
            'label' => 'Remote IP',
            'title' => 'Remote IP',
            'name' => 'remote_ip',
            'disabled' => true,
        ));

        $fieldset->addField('subject', 'text', array(
            'label' => 'Subject',
            'title' => 'Subject',
            'name' => 'subject',
            'disabled' => true,
        ));

        $fieldset->addField('other_subject', 'text', array(
            'label' => 'Other Subject',
            'title' => 'Other Subject',
            'name' => 'other_subject',
            'disabled' => true,
        ));

        $fieldset->addField('message_status', 'select', array(
            'name' => 'message_status',
            'label' => 'Message Status',
            'title' => 'Message Status',
            'values' => array(
                'unread' => 'unread',
                'read' => 'read',
                'spam' => 'spam',
            )
        ));
        $dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);

        $fieldset->addField('created_message', 'text', array(
            'label' => 'Created Message',
            'title' => 'Created Message',
            'name' => 'created_message',
            'disabled' => true,
            'input_format' => $dateFormatIso,
            'format'       => $dateFormatIso,
        ));

        $fieldset->addField('update_created_message', 'text', array(
            'label' => 'Update Message',
            'title' => 'Update Message',
            'name' => 'update_created_message',
            'disabled' => true,
            'input_format' => $dateFormatIso,
            'format'       => $dateFormatIso,
        ));

        $fieldset->addField('message', 'textarea', array(
            'name' => 'message',
            'label' => 'Message',
            'title' => 'Message',
            'disabled' => true,


        ));

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}