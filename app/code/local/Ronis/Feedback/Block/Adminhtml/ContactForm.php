<?php


class Ronis_Feedback_Block_Adminhtml_ContactForm extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_controller = 'adminhtml_feedback';
        $this->_blockGroup = 'feedback';
        $this->_headerText = 'Contact US';
        parent::__construct();
    }

}